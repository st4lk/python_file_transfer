File transfer with Bonjour
==========================

Simple tool for transmitting file over local network.
ZeroDNS is used to find ip and port of listening service.

Requirements
------------

- python 3.4+
- dependencies

        pip install -r requirements.txt

- mDNSResponder daemon

Usage
-----

There are two modes:

- as a service
- as a client

To run as a service, provide service name:

    python file_transfer.py ivan

To run as a client, provide already registered service name and path to file:

    python file_transfer.py ivan file_to_send.txt

To get avaliable options, run

    python file_transfer.py --help
