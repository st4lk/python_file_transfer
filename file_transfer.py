import sys
import os
import select
import socket
import time
import shutil
import pybonjour
import argparse
import functools

class TransferError(Exception):
    pass


class ServiceError(Exception):
    pass


class ProtocolSettings:
    """
    Protocol for file transfering:
    - first 2 bytes is length of filename, F_LENGTH
    - next F_LENGTH bytes is file name encoded in utf-8
    - next 6 bytes is number of bytes in file
    - following bytes is file content
    """
    filename_length_bytes = 2
    file_length_bytes = 6
    chunk_size = 8192
    encoding = 'utf8'


class ProgressBarMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.clear_progress_bar()

    def update_progress_bar(self, current, total):
        cols, rows = shutil.get_terminal_size()
        percent = int(current / total * 100)
        cols -= 5  # for showing percent
        if cols < 0:
            cols = 0
        cols_filled = int(current / total * cols)
        if cols_filled > self._prev_cols_shown or percent > self._prev_percent_shown:
            sys.stdout.write(
                "\r{0}".format('=' * cols_filled).ljust(cols) +
                "{0}%".format(percent).rjust(5)
            )
            self._prev_cols_shown = cols_filled
            self._prev_percent_shown = percent

    def clear_progress_bar(self):
        self._prev_cols_shown = 0
        self._prev_percent_shown = 0


class BonjourClient:
    """
    Client will search for service.
    When service is found, method `.service_found(host, port)` is called.
    Client must implement this method.

    Entry point: method `.run(...)`.
    """
    def __init__(self, *args, **kwargs):
        self.fds = []
        self.stop = False

    def service_found(self, host, port):
        raise NotImplementedError()

    def register_service_browse(self, service_name, regtype):
        return pybonjour.DNSServiceBrowse(regtype=regtype,
            callBack=functools.partial(self.browse_callback, service_name))

    def browse_callback(self, service_name_expected, sdRef, flags,
            interfaceIndex, errorCode, serviceName, regtype, replyDomain):
        if errorCode != pybonjour.kDNSServiceErr_NoError:
            return
        if not (flags & pybonjour.kDNSServiceFlagsAdd):
            return
        if serviceName != service_name_expected:
            return
        resolve_sdRef = pybonjour.DNSServiceResolve(0, interfaceIndex,
            serviceName, regtype, replyDomain, self.resolve_callback)
        if resolve_sdRef not in self.fds:
            self.fds.append(resolve_sdRef)

    def resolve_callback(self, sdRef, flags, interfaceIndex, errorCode,
            fullname, hosttarget, port, txtRecord):
        if errorCode == pybonjour.kDNSServiceErr_NoError:
            if not self.stop:
                try:
                    self.service_found(socket.gethostbyname(hosttarget), port)
                finally:
                    self.stop = True
        else:
            raise ServiceError(errorCode)

    def run(self, service_name, regtype, max_wait_time=5):
        """
        Search for service, run `.service_found(...)` and exit.
        If service is not found after `max_wait_time` seconds,
        ServiceError will be raised.
        """
        start_time = time.time()
        browse_fd = self.register_service_browse(service_name, regtype)
        self.fds = [browse_fd]
        try:
            while not self.stop:
                r, w, e = select.select(self.fds, [], [], 1)
                for fd in r:
                    pybonjour.DNSServiceProcessResult(fd)
                if not self.stop and time.time() - start_time > max_wait_time:
                    raise ServiceError("Can't find service with name '{0}'"
                        .format(service_name))
        finally:
            for fd in self.fds:
                fd.close()


class BonjourService:
    """
    Register service in ZeroDNS and listen for incoming connections.
    On connection, method `.handle_connection(client, addr)` will be called.
    Service must implement this method.

    Entry point: `.run(...)`.
    """
    def __init__(self, *args, **kwargs):
        pass

    def handle_connection(self, client, addr):
        """
        Be sure to close the client after work is done
        """
        raise NotImplementedError()

    def create_service_socket(self, host, port, max_clients=1):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind((host, port))
        s.listen(max_clients)
        return s

    def register_dns_service(self, service_name, regtype, port):
        sdRef = pybonjour.DNSServiceRegister(name=service_name,
                regtype=regtype, port=port,
                callBack=self.dns_service_registered_callback)
        return sdRef

    def dns_service_registered_callback(self, sdRef, flags, errorCode, name,
            regtype, domain):
        if errorCode == pybonjour.kDNSServiceErr_NoError:
            print("Successfully registered service: {0}".format(name))
        else:
            raise ServiceError(errorCode)

    def run(self, service_name, regtype, port, host='', max_clients=1):
        """
        Register service and listen for incoming connections.
        Connection is handled in blocking mode.
        """
        server_sock = self.create_service_socket(host, port, max_clients)
        service = self.register_dns_service(service_name, regtype, port)
        read_fds = [service, server_sock]
        try:
            while True:
                r, w, e = select.select(read_fds, [], [])
                if service in r:
                    pybonjour.DNSServiceProcessResult(service)
                if server_sock in r:
                    client, addr = server_sock.accept()
                    self.handle_connection(client, addr)
        finally:
            for fd in read_fds:
                fd.close()


class FileClient(ProgressBarMixin, BonjourClient):

    def __init__(self, protocol_settings, file_path, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.file_path = file_path
        self.protocol_settings = protocol_settings

    def service_found(self, host, port):
        sock = socket.socket()
        ps = self.protocol_settings
        try:
            print("Connecting with {0}:{1}".format(host, port))
            sock.connect((host, port))
            _, file_name = os.path.split(self.file_path)
            # send file name size
            file_name_encoded = file_name.encode(ps.encoding)
            if len(file_name_encoded) > int('ff' * ps.filename_length_bytes, 16):
                raise TransferError("File name too big")
            sock.sendall(self.hex_encode(len(file_name_encoded), ps.filename_length_bytes))
            # send file name
            sock.sendall(file_name_encoded)
            # send file size
            f_size = os.path.getsize(self.file_path)
            sock.sendall(self.hex_encode(f_size, ps.file_length_bytes))
            # send file
            with open(self.file_path, 'rb') as f:
                data = f.read(ps.chunk_size)
                total_send = len(data)
                while data:
                    sock.sendall(data)
                    self.update_progress_bar(total_send, f_size)
                    data = f.read(ps.chunk_size)
                    total_send += len(data)
                print("File sent", file_name)
        finally:
            sock.close()
            self.clear_progress_bar()

    def hex_encode(self, value, length):
        result = bytearray()
        for i in range(length):
            result.append(value & 255)
            value >>= 8
        return result


class FileService(ProgressBarMixin, BonjourService):

    def __init__(self, protocol_settings, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.protocol_settings = protocol_settings

    def handle_connection(self, client, addr):
        print("Client connected from {0}".format(addr))
        total_data = bytearray()
        ps = self.protocol_settings
        try:
            # receive file name length
            fname_len_bytes = self.receive_bytes(client, total_data,
                ps.filename_length_bytes, ps.chunk_size)
            fname_len = self.hex_decode(fname_len_bytes)
            total_data = total_data[ps.filename_length_bytes:]
            # receive file name
            filename = self.receive_bytes(client, total_data, fname_len, ps.chunk_size)
            filename = filename.decode(ps.encoding)
            total_data = total_data[fname_len:]
            print("Receiving file {0}".format(filename))
            # receive file size
            total_file_size_bytes = self.receive_bytes(client, total_data,
                ps.file_length_bytes, ps.chunk_size)
            total_file_size = self.hex_decode(total_file_size_bytes)
            total_data = total_data[ps.file_length_bytes:]
            # receive file
            unused_filename = self.get_unused_filename(filename)
            with open(unused_filename, 'wb') as f:
                total_received = len(total_data)
                if total_data:
                    f.write(total_data)
                while True:
                    self.update_progress_bar(total_received, total_file_size)
                    data = client.recv(ps.chunk_size)
                    if not data:
                        break
                    f.write(data)
                    total_received += len(data)
            if total_received < total_file_size:
                raise TransferError("File not received fully")
            print("Received {0}, saved as {1}".format(filename, unused_filename))
        except TransferError as e:
            print(";".join(e.args))
        finally:
            client.close()
            self.clear_progress_bar()

    def receive_bytes(self, client, to_byte_array, byte_amount, chunk_size):
        while len(to_byte_array) < byte_amount:
            data = client.recv(chunk_size)
            if not data:
                raise TransferError("Bad data from client")
            to_byte_array += data
        return to_byte_array[:byte_amount]

    def hex_decode(self, barray):
        result = 0
        for i in range(len(barray)):
            result += barray[i] << (i * 8)
        return result

    def get_unused_filename(self, file_name):
        name, ext = os.path.splitext(file_name)
        count = ''
        while os.path.isfile('%s%s%s' % (name, count, ext)):
            count = int(count or "0") + 1
        return '%s%s%s' % (name, count, ext)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("servicename", help="ZeroDNS service name")
    parser.add_argument("filename", nargs='?', default=None,
        help="Path to file to be send. If omitted, run as server")
    parser.add_argument("-p", "--port", default=25000, type=int,
        help="Port to use in server mode")
    parser.add_argument("-t", "--type", default='_file._tcp',
        help="Service type")
    args = parser.parse_args()
    if args.filename:
        client = FileClient(ProtocolSettings, args.filename)
        client.run(args.servicename, args.type)
    else:
        server = FileService(ProtocolSettings)
        server.run(args.servicename, args.type, args.port)
